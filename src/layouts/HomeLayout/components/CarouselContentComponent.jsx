import UIButton from "../../../widgets/UIButton/UIButton";
import UIImage from "../../../widgets/UIImage/UIImage";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import ButtonStyles from "../../../widgets/UIButton/style.module.css";
import HomeBannerStyles from "../../../styles/HomeBanner.module.css";

const CarouselContentComponent = ({ image, heading, content }) => {
  return (
    <div className={HomeBannerStyles.carousel_Img}>
      <UIImage imgsrc={image} className={HomeBannerStyles.img} />
      <div className={HomeBannerStyles.carousel_content}>
        <h1>{heading}</h1>
        <p>{content}</p>
        <UIButton title="Know more" />
      </div>
    </div>
  );
};

export default CarouselContentComponent;
