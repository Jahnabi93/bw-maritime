import React from "react";
import { Carousel } from "rsuite";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import CarouselContentComponent from "./CarouselContentComponent";
import HomeBannerStyles from "../../../styles/HomeBanner.module.css";

let items = [
  {
    id: 1,
    heading: "Example headline.",
    content:
      "Some representative placeholder content for the first slide of the carousel.",
    image: "images/1.jpg",
  },
  {
    id: 2,
    heading: "Example headline.",
    content:
      "Some representative placeholder content for the second slide of the carousel.",
    image: "images/1.jpg",
  },
  {
    id: 3,
    heading: "Example headline.",
    content:
      "Some representative placeholder content for the third slide of the carousel.",
    image: "images/1.jpg",
  },
];
export default function CarouselComponent() {
  const [placement] = React.useState("bottom");
  const [shape] = React.useState("bar");

  return (
    <>
      <Carousel
        key={`${placement}.${shape}`}
        placement={placement}
        shape={shape}
        autoplay
      >
        {items.map((item) => (
          <div key={item.id}>
            <CarouselContentComponent
              image={item.image}
              heading={item.heading}
              content={item.content}
              className={HomeBannerStyles.carouselImg}
            />
          </div>
        ))}
      </Carousel>
    </>
  );
}
