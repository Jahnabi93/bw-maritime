import UIImage from "../../../widgets/UIImage/UIImage";
import CentralDivComponent from "../../ContactLayout/components/CentralDivComponent";
import HomeBannerStyles from "../../../styles/HomeBanner.module.css"
import ContainerStyles from "../../../widgets/UIContainer/style.module.css"

const Location = () => {
  return (
    <>
      <div className={HomeBannerStyles.location_img}>
        <UIImage imgsrc="images/map.jpg" />
        <CentralDivComponent/>
      </div>
    </>
  );
};

export default Location;
