import { Divider } from "rsuite";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import HomeBannerStyles from "../../../styles/HomeBanner.module.css";

let items = [
  {
    id: 1,
    name: "Toronto",
    location: "Suite 360-144 Front Street West, Toronto, Ontario, M5J 2L7",
  },
  {
    id: 2,
    name: "Netherlands",
    location: "Suite 360-144 Front Street West, Toronto, Ontario, M5J 2L7",
  },
  {
    id: 3,
    name: "San Francisco",
    location: "Suite 360-144 Front Street West, Toronto, Ontario, M5J 2L7",
  },
];
const CentralDivComponent = () => {
  return (
    <>
      <div className={HomeBannerStyles.central_div}>
        <UIRow>
          {items.map((item) => (
            <UICol xs={24} md={8} key={item.id}>
              <h5>{item.name}</h5>
              <p>
                {item.location}
                <Divider vertical />
              </p>
            </UICol>
          ))}
        </UIRow>
      </div>
    </>
  );
};

export default CentralDivComponent;
