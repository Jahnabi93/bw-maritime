import HeaderComponent from "../../../components/common/HeaderComponent";
import { Divider } from "rsuite";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import ContainerStyles from "../../../widgets/UIContainer/style.module.css";

let items = [
  {
    id: 1,
    heading: "MESSAGE US",
    content: "Start a chat!",
  },
  {
    id: 2,
    heading: "CALL ANYTIME",
    content: "(555) 123-4567",
  },
  {
    id: 3,
    heading: "EMAIL US",
    content: "support@goodthemes.co",
  },
];

const ContactContent = () => {
  return (
    <>
      <UIContainer className={ContainerStyles.ContactContent}>
        <HeaderComponent
          heading="we are located at"
          content="contact content"
        />
        <UIRow>
          {items.map((item) => (
            <UICol key={item.id} xs={24} sm={8}>
              <h5>{item.heading}</h5>
              <p>
                {item.content}
                <Divider vertical/>
              </p>
            </UICol>
          ))}
        </UIRow>
      </UIContainer>
    </>
  );
};

export default ContactContent;
