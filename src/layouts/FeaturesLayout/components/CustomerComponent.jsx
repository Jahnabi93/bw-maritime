import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import UIImage from "../../../widgets/UIImage/UIImage";
import HomeBannerStyles from "../../../styles/HomeBanner.module.css"

const CustomerComponent = ({ image, logo, content, name }) => {
  return (
    <>
      <UIRow>
        <UICol xs={24} md={12}>
          <UIImage imgsrc={image}/>
        </UICol>
        <UICol xs={24} md={12}>
          {logo}
          <p>{content}</p>
          <p>{name}</p>
        </UICol>
      </UIRow>
    </>
  );
};

export default CustomerComponent;
