import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import FeaturesComponent from "./FeaturesComponent";
import HeaderComponent from "../../../components/common/HeaderComponent";
import UIContainer from "../../../widgets/UIContainer/UIContainer";

let features = [
  {
    id: 1,
    heading: "Lorem ipsum dolor",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
    image: "images/box.jpg",
  },
  {
    id: 2,
    heading: "Lorem ipsum dolor",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
    image: "images/box.jpg",
  },
  {
    id: 3,
    heading: "Lorem ipsum dolor",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
    image: "images/box.jpg",
  },
  {
    id: 4,
    heading: "Lorem ipsum dolor",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
    image: "images/box.jpg",
  },
];

const FeaturesListingComponent = () => {
  return (
    <>
      <UIContainer>
        <HeaderComponent
          heading="We have lots of shipping experience"
          content="Features content"
        />
        {features.map((feature) => (
          <UICol xs={24} md={12} key={feature.id}>
            <FeaturesComponent
              image={feature.image}
              heading={feature.heading}
              content={feature.content}
            />
          </UICol>
        ))}
      </UIContainer>
    </>
  );
};

export default FeaturesListingComponent;
