import { Carousel } from "rsuite";
import CustomerComponent from "./CustomerComponent";
import HeaderComponent from "../../../components/common/HeaderComponent";
import UIContainer from "../../../widgets/UIContainer/UIContainer";

let customers = [
  {
    id: 1,
    image: "images/girl1.jpg",
    logo: "LOGO",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
    name: "DAVE GAMACHE",
  },
  { id: 2, image: "", logo: "", content: "", name: "" },
  { id: 3, image: "", logo: "", content: "", name: "" },
  { id: 4, image: "", logo: "", content: "", name: "" },
];

const CustomerListingComponent = () => {
  return (
    <UIContainer>
      <HeaderComponent
        heading="Our customers are our biggest fans."
        content="Customer content"
      />
      <Carousel>
        {customers.map((customer) => (
          <div key={customer.id}>
            <CustomerComponent
              image={customer.image}
              logo={customer.logo}
              content={customer.content}
              name={customer.name}
            />
          </div>
        ))}
      </Carousel>
    </UIContainer>
  );
};

export default CustomerListingComponent;
