import UIImage from "../../../widgets/UIImage/UIImage";
import UICol from "../../../widgets/UICol";
import UIRow from "../../../widgets/UIRow";
import HomeBannerStyles from "../../../styles/HomeBanner.module.css";
import UIContainer from "../../../widgets/UIContainer/UIContainer";

const FeaturesComponent = ({ image, heading, content }) => {
  return (
    <>
      <UIContainer>
        <UIRow>
          <UICol xs={12}>
            <UIImage imgsrc={image} />
          </UICol>
          <UICol xs={12}>
            <h5>{heading}</h5>
            <p>{content}</p>
          </UICol>
        </UIRow>
      </UIContainer>
    </>
  );
};

export default FeaturesComponent;
