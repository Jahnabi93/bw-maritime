import JobOpenings from "./JobOpenings";
import { Divider } from "rsuite";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import UIButton from "../../../widgets/UIButton/UIButton";
import ButtonStyles from "../../../widgets/UIButton/style.module.css";

let items = [
  {
    id: 1,
    role: "Senior UX Designer",
    description: "Responsible for design systems and brand management.",
    team: "Consumer",
    location: "Los Angeles",
  },
  {
    id: 2,
    role: "Motion Designer",
    description: "Responsible for creating life in our apps.",
    team: "Product",
    location: "San Francisco,CA",
  },
  {
    id: 3,
    role: "Design Researcher",
    description:
      "Help us make the best decisions with qualitative experiments.",
    team: "Consulting",
    location: "London",
  },
  {
    id: 4,
    role: "Production Designer",
    description: "Create, collect, and distribute beautiful assets.",
    team: "Consulting",
    location: "Paris",
  },
];

const JobOpeningListing = () => {
  return (
    <UIContainer>
      <h5>Job openings</h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, </p>
      <UIRow>
        <UICol xs={24} sm={12}>
          <h5>Role</h5>
        </UICol>
        <UICol xs={24} sm={6}>
          <h5>Team</h5>
        </UICol>
        <UICol xs={24} sm={6}>
          <h5>Location</h5>
        </UICol>
        <Divider />
      </UIRow>
      {items.map((item) => (
        <JobOpenings
          key={item.id}
          role={item.role}
          description={item.description}
          team={item.team}
          location={item.location}
        />
      ))}
      <div className={ButtonStyles.center}>
        <UIButton title="View all open positions &#x2192;" />
      </div>
    </UIContainer>
  );
};

export default JobOpeningListing;
