import CareerDropdown from "./CareerDropdown";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import HeaderComponent from "../../../components/common/HeaderComponent";

let items = [
  {
    id: 1,
    heading: "Roles",
    title: "All roles",
  },
  {
    id: 2,
    heading: "Team",
    title: "All teams",
  },
  {
    id: 3,
    heading: "Location",
    title: "All locations",
  },
];

const CareerDropdownListing = () => {
  return (
    <UIContainer>
      <HeaderComponent heading="Let’s find you an open position." content="Career Content"/>
      <UIRow>
        {items.map((item) => (
          <UICol xs={24} sm={8} key={item.id}>
            <h5>{item.heading}</h5>
            <CareerDropdown id={item.id} title={item.title} />
          </UICol>
        ))}
      </UIRow>
    </UIContainer>
  );
};

export default CareerDropdownListing;
