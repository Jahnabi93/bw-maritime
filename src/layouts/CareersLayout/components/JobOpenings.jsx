import UICol from "../../../widgets/UICol";
import UIRow from "../../../widgets/UIRow";

const JobOpenings = ({ role, description, team, location }) => {
  return (
    <>
      <UIRow>
        <UICol xs={24} sm={12}>
          <h5>{role}</h5>
          <UICol>
            <p>{description}</p>
          </UICol>
        </UICol>
        <UICol xs={24} sm={6}>
          <h5>{team}</h5>
        </UICol>
        <UICol xs={24} sm={6}>
          <h5>{location}</h5>
        </UICol>
      </UIRow>
    </>
  );
};

export default JobOpenings;
