import { Dropdown, ButtonToolbar } from "rsuite";

let SizeDropdown = (props) => (
  <Dropdown appearance="default" {...props}>
    <Dropdown.Item>New File</Dropdown.Item>
    <Dropdown.Item>New File with Current Profile</Dropdown.Item>
    <Dropdown.Item>Download As...</Dropdown.Item>
    <Dropdown.Item>Export PDF</Dropdown.Item>
  </Dropdown>
);

const CareerDropdown = ({title}) => {
  return (
    <>
      <ButtonToolbar>
        <SizeDropdown title={title} size="md" />
      </ButtonToolbar>
    </>
  );
};

export default CareerDropdown;
