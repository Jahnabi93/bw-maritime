import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import UIButton from "../../../widgets/UIButton/UIButton";
import BlogsComponent from "../../BlogsLayout/components/BlogsComponent";
import HeaderComponent from "../../../components/common/HeaderComponent";
import ButtonStyles from "../../../widgets/UIButton/style.module.css";

let items = [
  {
    id: 1,
    image: "images/blog3.jpg",
    heading: "Lorem ipsum dolor sit amet, consectetur.",
    content:
      "Lorem ipsum dolor sit amet, consectetue adipiscing elit. Duis nec condimentum quam.",
    profile: "images/profile1.jpg",
    name: "David John",
    date: "May 02",
  },
  {
    id: 2,
    image: "images/blog1.jpg",
    heading: "Lorem ipsum dolor sit amet, consectetur.",
    content:
      "Lorem ipsum dolor sit amet, consectetue adipiscing elit. Duis nec condimentum quam.",
    profile: "images/profile2.jpg",
    name: "David John",
    date: "May 02",
  },
  {
    id: 3,
    image: "images/blog2.jpg",
    heading: "Lorem ipsum dolor sit amet, consectetur.",
    content:
      "Lorem ipsum dolor sit amet, consectetue adipiscing elit. Duis nec condimentum quam.",
    profile: "images/profile3.jpg",
    name: "David John",
    date: "May 02",
  },
];
const BlogsListingComponent = () => {
  return (
    <UIContainer>
      <HeaderComponent
        heading="Latest stories what we've been."
        content="Blogs content"
      />
      <UIRow>
        {items.map((item) => (
          <UICol xs={24} md={8} key={item.id}>
            <BlogsComponent
              id={item.id}
              image={item.image}
              heading={item.heading}
              content={item.content}
              profile={item.profile}
              name={item.name}
              date={item.date}
            />
          </UICol>
        ))}
      </UIRow>
      <div className={ButtonStyles.center}>
        <UIButton title="View Blogs &#x2192;" />
      </div>
    </UIContainer>
  );
};

export default BlogsListingComponent;
