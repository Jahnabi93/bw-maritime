import UICol from "../../../widgets/UICol";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIImage from "../../../widgets/UIImage/UIImage";
import UIRow from "../../../widgets/UIRow";
import ContainerStyles from "../../../widgets/UIContainer/style.module.css";

const BlogsComponent = ({ image, heading, content, profile, name, date }) => {
  return (
    <div className={ContainerStyles.BlogsComponent}>
      <UIImage imgsrc={image} />
      <UIContainer className={ContainerStyles.text_left}>
        <UIRow>
          <h5>{heading}</h5>
          <p>{content}</p>
        </UIRow>
        <UIRow>
          <UICol xs={12}>
            <UIImage imgsrc={profile} />
            {name}
          </UICol>
          <UICol xs={12}>{date}</UICol>
        </UIRow>
      </UIContainer>
    </div>
  );
};

export default BlogsComponent;
