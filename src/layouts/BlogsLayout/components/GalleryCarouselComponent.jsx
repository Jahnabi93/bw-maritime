import React from "react";
import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIImage from "../../../widgets/UIImage/UIImage";
import { Carousel } from "rsuite";
import HeaderComponent from "../../../components/common/HeaderComponent";

function GalleryCarouselComponent() {
  const [placement] = React.useState("bottom");
  const [shape] = React.useState("dot");
  return (
    <UIContainer>
      <HeaderComponent heading="Check out gallery." content="Gallery content"/>
      <Carousel
        key={`${placement}.${shape}`}
        placement={placement}
        shape={shape}
        className="custom-slider"
      >
        <UIImage imgsrc="images/img1.jpg"/>
        <UIImage imgsrc="images/img1.jpg"/>
        <UIImage imgsrc="images/img1.jpg"/>
        <UIImage imgsrc="images/img1.jpg"/>
        <UIImage imgsrc="images/img1.jpg"/>
      </Carousel>
      </UIContainer>
  );
}

export default GalleryCarouselComponent;
