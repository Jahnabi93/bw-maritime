import { Divider } from "rsuite";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import UIImage from "../../../widgets/UIImage/UIImage";
import UIContainer from "../../../widgets/UIContainer/UIContainer";

const AboutShippingComponent = () => {
  return (
    <UIContainer>
      <UIRow>
        <UICol xs={24} md={12}>
          <h1>Stay focused on your business.</h1>
          <h2>Let us handle the shipping.</h2>
          <p>
            You have a business to run. Stop worring about cross-browser bugs,
            designing new pages, keeping your components up to date. Let us do
            that for you.
          </p>
          <UIRow>
            <UICol xs={8}>
              100%
              <br />
              Satisfaction&nbsp;&nbsp;
              <Divider vertical />
            </UICol>
            <UICol xs={8}>
              24/7
              <br />
              Support&nbsp;&nbsp;
              <Divider vertical />
            </UICol>
            <UICol xs={8}>
              100k+
              <br />
              Sales&nbsp;&nbsp;
              <Divider vertical />
            </UICol>
          </UIRow>
        </UICol>
        <UICol xs={24} md={12}>
          <UIImage imgsrc="images/illustrations.jpg" />
        </UICol>
      </UIRow>
    </UIContainer>
  );
};

export default AboutShippingComponent;
