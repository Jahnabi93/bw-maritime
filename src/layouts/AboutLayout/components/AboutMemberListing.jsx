import UIContainer from "../../../widgets/UIContainer/UIContainer";
import UIImage from "../../../widgets/UIImage/UIImage";
import UIRow from "../../../widgets/UIRow";
import UICol from "../../../widgets/UICol";
import UIButton from "../../../widgets/UIButton/UIButton";
import HeaderComponent from "../../../components/common/HeaderComponent";
import ButtonStyles from "../../../widgets/UIButton/style.module.css";

const AboutMemberListing = () => {
  return (
    <UIContainer>
      <HeaderComponent
        heading="Check out all our team"
        content="About content"
      />
      <UIRow>
        <UICol xs={8}>
          <UIImage imgsrc="images/a6.jpg"/>
        </UICol>
        <UICol xs={8}>
          <UIImage imgsrc="images/a5.jpg"/>
          <UIImage imgsrc="images/a2.jpg" style={{ marginTop: "50px"}}/>
        </UICol>
        <UICol xs={8}>
          <UIImage imgsrc="images/a4.jpg" />
          <UIImage imgsrc="images/a3.jpg" />
          <br />
          <UIImage imgsrc="images/a1.jpg" />
        </UICol>
      </UIRow>
      <div className={ButtonStyles.center}>
        <UIButton title="View our team &#x2192;" />
      </div>
    </UIContainer>
  );
};

export default AboutMemberListing;
