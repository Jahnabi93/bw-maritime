import UIContainer from "../../widgets/UIContainer/UIContainer";

const ServicesComponent=({ heading, content })=> {
  return (
    <UIContainer>
      <h5>{heading}</h5>
      <p>{content}</p>
    </UIContainer>
  );
}

export default ServicesComponent;
