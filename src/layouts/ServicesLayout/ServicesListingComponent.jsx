import ServicesComponent from "./ServicesComponent";
import UIRow from "../../widgets/UIRow";
import UICol from "../../widgets/UICol";
import UIButton from "../../widgets/UIButton/UIButton";
import UIImage from "../../widgets/UIImage/UIImage";
import ButtonStyles from "../../widgets/UIButton/style.module.css";
import UIContainer from "../../widgets/UIContainer/UIContainer";
import HomeStyles from "../../styles/HomeBanner.module.css";
import NavItem from "rsuite/lib/Nav/NavItem";

let services = [
  {
    id: 1,
    heading: "Services01",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
  },
  {
    id: 2,
    heading: "Services02",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
  },
  {
    id: 3,
    heading: "Services03",
    content:
      "Lorem ipsum dolor sit amet, consectetur.adipiscing elit, sed do eiusmod tempor,incididunt ut labore et dolore.",
  },
];

const ServicesListingComponent = () => {
  return (
    <>
      <div className={HomeStyles.services}>
        <UIContainer>
          <UIRow>
            {services.map((service) => (
              <UICol xs={24} md={12} lg={8} key={service.id}>
                <UIContainer>
                  <UIImage imgsrc="images/icon.jpg" />
                </UIContainer>
                <ServicesComponent
                  id={service.id}
                  heading={service.heading}
                  content={service.content}
                />
              </UICol>
            ))}
          </UIRow>
          <div className={ButtonStyles.center}>
            <UIButton title="View Services &#x2192;" />
          </div>
        </UIContainer>
      </div>
    </>
  );
};

export default ServicesListingComponent;
