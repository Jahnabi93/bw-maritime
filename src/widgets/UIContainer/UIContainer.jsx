import React from "react";
import ContainerStyles from "../UIContainer/style.module.css";

const UIContainer = ({ children }) => {
  return <div className={ContainerStyles.container}>{children}</div>;
};

export default UIContainer;
