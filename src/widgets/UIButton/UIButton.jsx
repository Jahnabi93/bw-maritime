import { Button } from "rsuite";

const UIButton = ({title, onClick}) => {
  return (
    <div>
      <Button color="blue" onClick={()=> onClick()}>{title}</Button>
    </div>
  );
};

export default UIButton;
