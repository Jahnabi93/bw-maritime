import { Row } from "rsuite";

const UIRow = ({ children }) => {
  return <Row>{children}</Row>;
};

export default UIRow;
