import { Col } from "rsuite";

const UICol = ({ xs, sm, md, lg, xl, children }) => {
  return (
    <Col xs={xs} sm={sm} md={md} lg={lg} xl={xl}>
      {children}
    </Col>
  );
};

export default UICol;
