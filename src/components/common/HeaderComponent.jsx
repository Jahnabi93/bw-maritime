import UIContainer from "../../widgets/UIContainer/UIContainer";
import HomeStyles from "../../styles/HomeBanner.module.css";

const HeaderComponent = (props) => {
  return (
    <div className={HomeStyles.header}>
      <UIContainer>
        <h5>{props.heading}</h5>
        <p>{props.content}</p>
      </UIContainer>
    </div>
  );
};

export default HeaderComponent;
