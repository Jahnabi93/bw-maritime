import UIRow from "../../widgets/UIRow";
import UICol from "../../widgets/UICol";
import FooterStyles from "../../styles/footer.module.css";
import { Icon } from "rsuite";

const FooterComponent = () => {
  return (
    <>
      <div className={FooterStyles.container}>
        <UIRow>
          <UICol xs={24} md={12} lg={4}>
            <span className={FooterStyles.header}>
              <b>Blue Whale</b> Maritime
            </span>
            <br />A better way to ship.
            <UICol className="pt-3">
              <Icon icon="facebook" />
              &nbsp;&nbsp;
              <Icon icon="twitter" />
              &nbsp;&nbsp;
              <Icon icon="youtube-play" />
              &nbsp;&nbsp;
              <Icon icon="linkedin" />
              &nbsp;&nbsp;
              <Icon icon="instagram" />
              &nbsp;&nbsp;
            </UICol>
          </UICol>

          <UICol xs={24} md={12} lg={4}>
            <span className={FooterStyles.header}>PRODUCTS</span>
            <br />
            Lorem Ipsum
            <br />
            Lorem
            <br />
            Lorem Ipsum
            <br />
            Lorem
          </UICol>
          <UICol xs={24} md={12} lg={4}>
            <span className={FooterStyles.header}>SERVICES</span>
            <br />
            Lorem
            <br />
            Lorem Ipsum
            <br />
            Lorem
            <br />
            Lorem Ipsum
          </UICol>
          <UICol xs={24} md={12} lg={4}>
            <span className={FooterStyles.header}>COMPANY</span>
            <br />
            Lorem Ipsum
            <br />
            Lorem
            <br />
            Lorem Ipsum
            <br />
            Lorem
          </UICol>
          <UICol xs={24} md={12} lg={4}>
            <span className={FooterStyles.header}>LEGAL</span>
            <br />
            Lorem
            <br />
            Lorem Ipsum
            <br />
            Lorem
            <br />
            Lorem Ipsum
          </UICol>
        </UIRow>
        <div className={FooterStyles.container2}>
          <UIRow>
            <UICol xs={24} md={12}>&copy; Blue Whale Maritime Pvt. Ltd.</UICol>
            <UICol xs={24} md={12}>
              <UICol>Privacy&nbsp;&nbsp; Cookie Policy&nbsp;&nbsp; Terms</UICol>
            </UICol>
          </UIRow>
        </div>
      </div>
    </>
  );
};

export default FooterComponent;
