import { Navbar, Nav, FlexboxGrid } from "rsuite";
import React, { Component } from "react";
import Link from "next/link";
import UIButton from "../../widgets/UIButton/UIButton";

const NavBarInstance = ({ onSelect, activeKey, ...props }) => {
  return (
    <Navbar {...props}>
      <Navbar.Body>
        <Nav onSelect={onSelect} activeKey={activeKey}>
          <Nav.Item eventKey="1">
            <b>Blue Whale</b> Maritime
          </Nav.Item>
        </Nav>
        <Nav>
          <Nav.Item eventKey="2">
            <Link href="/">Home</Link>
          </Nav.Item>
          <Nav.Item eventKey="3">
            <Link href="/services">Services</Link>
          </Nav.Item>
          <Nav.Item eventKey="4">
            <Link href="/features">Features</Link>
          </Nav.Item>
          <Nav.Item eventKey="5">
            <Link href="/about">About</Link>
          </Nav.Item>
          <Nav.Item eventKey="6">
            <Link href="/blogs">Blogs</Link>
          </Nav.Item>
        </Nav>
        <Nav pullRight>
          <Nav.Item eventKey="7">
            <Link href="/careers">Careers</Link>
          </Nav.Item>
          <Nav.Item eventKey="8">
            <Link href="/contact">
              <UIButton title="Contact us"/>
            </Link>
          </Nav.Item>
        </Nav>
      </Navbar.Body>
    </Navbar>
  );
};

class NavbarComponent extends Component {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = {
      activeKey: null,
    };
  }
  handleSelect(eventKey) {
    this.setState({
      activeKey: eventKey,
    });
  }
  render() {
    const { activeKey } = this.state;
    return (
      <div className="nav-wrapper">
        <NavBarInstance
          appearance="subtle"
          activeKey={activeKey}
          onSelect={this.handleSelect}
        />
      </div>
    );
  }
}

export default NavbarComponent;
