import CarouselComponent from "../../layouts/HomeLayout/components/CarouselComponent";
import ServicesListingComponent from "../../layouts/ServicesLayout/ServicesListingComponent";
import FeaturesListingComponent from "../../layouts/FeaturesLayout/components/FeaturesListingComponent";
import CustomerListingComponent from "../../layouts/FeaturesLayout/components/CustomerListingComponent";
import AboutMemberListing from "../../layouts/AboutLayout/components/AboutMemberListing";
import AboutShippingComponent from "../../layouts/AboutLayout/components/AboutShippingComponent";
import GalleryCarouselComponent from "../../layouts/BlogsLayout/components/GalleryCarouselComponent";
import BlogsListingComponent from "../../layouts/BlogsLayout/components/BlogsListingComponent";
import CareerDropdownListing from "../../layouts/CareersLayout/components/CareerDropdownListing";
import JobOpeningListing from "../../layouts/CareersLayout/components/JobOpeningListing";
import ContactContent from "../../layouts/ContactLayout/components/ContactContent";
import Location from "../../layouts/ContactLayout/components/location";

const HomeBannerComponent = () => {
  return (
    <div>
      <main>
        <CarouselComponent />
        <ServicesListingComponent />
        <FeaturesListingComponent />
        <CustomerListingComponent />
        <AboutMemberListing />
        <AboutShippingComponent />
        <GalleryCarouselComponent />
        <BlogsListingComponent />
        <CareerDropdownListing />
        <JobOpeningListing />
        <Location />
        <ContactContent />
      </main>
    </div>
  );
};

export default HomeBannerComponent;
