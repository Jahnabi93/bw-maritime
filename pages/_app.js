import "rsuite/dist/styles/rsuite-default.css";
import 'bootstrap/dist/css/bootstrap.css';


function MyApp({ Component, pageProps }) {
    return (
      <>
        <Component {...pageProps} />
      </>
    );
  }
  
  export default MyApp;
  