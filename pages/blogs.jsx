import React from 'react';
import FooterComponent from '../src/components/common/FooterComponent';
import NavbarComponent from '../src/components/common/NavbarComponent';

const blogs = () => {
    return (
        <div>
            <NavbarComponent/>
            Blogs
            <FooterComponent/>
        </div>
    );
}

export default blogs;
