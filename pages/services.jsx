import React from 'react';
import FooterComponent from '../src/components/common/FooterComponent';
import NavbarComponent from '../src/components/common/NavbarComponent';

const services = () => {
    return (
        <div>
            <NavbarComponent/>
            Services Page
            <FooterComponent/>
        </div>
    );
}

export default services;
