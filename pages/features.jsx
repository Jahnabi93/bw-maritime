import React from 'react';
import FooterComponent from '../src/components/common/FooterComponent';
import NavbarComponent from '../src/components/common/NavbarComponent';

const features = () => {
    return (
        <div>
            <NavbarComponent/>
            Features
            <FooterComponent/>
        </div>
    );
}

export default features;
