import React from 'react';
import FooterComponent from '../src/components/common/FooterComponent';
import NavbarComponent from '../src/components/common/NavbarComponent';

const about = () => {
    return (
        <div>
            <NavbarComponent/>
            About
            <FooterComponent/>
        </div>
    );
}

export default about;
