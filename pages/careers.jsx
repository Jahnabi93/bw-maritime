import React from 'react';
import FooterComponent from '../src/components/common/FooterComponent';
import NavbarComponent from '../src/components/common/NavbarComponent';

const careers = () => {
    return (
        <div>
            <NavbarComponent/>
            Careers
            <FooterComponent/>
        </div>
    );
}

export default careers;
