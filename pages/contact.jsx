import React from 'react';
import FooterComponent from '../src/components/common/FooterComponent';
import NavbarComponent from '../src/components/common/NavbarComponent';

const contact = () => {
    return (
        <div>
            <NavbarComponent/>
            Contact
            <FooterComponent/>
        </div>
    );
}

export default contact;
