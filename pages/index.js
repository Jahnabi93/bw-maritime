import Head from "next/head";
import NavbarComponent from "../src/components/common/NavbarComponent";
import FooterComponent from "../src/components/common/FooterComponent";
import HomeBannerComponent from "../src/components/banners/HomeBannerComponent";

export default function Home() {
  return (
    <>
      <NavbarComponent />
      <div>
        <Head>
          <title>BW Maritime</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <HomeBannerComponent />
        <FooterComponent />

        <style jsx global>{`
          html,
          body {
            padding: 0px;
            margin: 0px;
            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
              Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
              sans-serif;
              overflow-x: hidden;
              width: 100%;
              height: 100%;
          }

          * {
            box-sizing: border-box;
          }
        `}</style>
      </div>
    </>
  );
}
